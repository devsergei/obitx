<?php
namespace Obitx;
/**
 * news
 *
 */
class news {
  public function get_feed_news( $page = 0, $date = '', $limit = 5 ) {
    $ctx = stream_context_create(array('http' => array('timeout' => 15)));
    $articles = '';
    $articles_arr = [];

    $url = 'https://mciggroup.com/json420news.php';

    if ( ! empty( $date ) ) {
      $url .= '?orderby%5B%5D=date_desc&date_query=&y=' . $date;
    } else {
      $url .= '?orderby%5B%5D=date_desc&date_query=';
    }

    if ( ! empty( $page ) && $page > 0 ) {
      $url .= '&page=' . $page;
    }

    $articles = @file_get_contents( $url, 0, $ctx );

    if ( ! empty( $articles ) ) {
      $articles = json_decode( $articles );
      $articles->prev_page = ( $articles->current_page_index - 1 ) >= 1 ? ( $articles->current_page_index - 1 ) : 0;
      $articles->next_page = $articles->next_page_index;
    
      if ( ! empty( $articles->data ) ) {
        foreach ( $articles->data as $ar_k => $ar_v ) {
          $articles_arr[ $ar_k ] = $this->getPost( $ar_v );
          $articles_arr[ $ar_k ]->share = $this->generateShareLinks( $articles_arr[ $ar_k ]->share );
        }
        $articles->data = $articles_arr;
      }

      $articles->page_prev_link = '/investors/' . $articles->prev_page . '/';
      $articles->page_prev_link .= ! empty( $date ) ? $date . '/' : '';

      $articles->next_page_link = '/investors/' . $articles->next_page . '/';
      $articles->next_page_link .= ! empty( $date ) ? $date . '/' : '';
    }
    
    return $articles;
  }

  public function get_feed_single_news( $post_id ) {
    $ctx = stream_context_create(array('http' => array('timeout' => 10)));
    $article = '';
    $article_latest = '';
    $article = @file_get_contents('https://mciggroup.com/json420news.php?post_id=' . $post_id, 0, $ctx);
    
    if ( ! empty( $article ) ) {
      $article = json_decode( $article );

      if ( ! empty( $article->data ) ) {
        $article = $this->getPost( $article->data[0] );
        $article->share = $this->generateShareLinks( $article->share );
        
        if ( ! empty( $article->prev_post ) ) {
          $prev_article = @file_get_contents('https://mciggroup.com/json420news.php?post_id=' . $article->prev_post, 0, $ctx);
          $prev_article = json_decode( $prev_article );
          if ( ! empty( $prev_article ) && ! empty( $prev_article->data ) ) {
            $article->prev_post_slug = $this->slugify( $prev_article->data[0]->title );
          }
        }

        if ( ! empty( $article->next_post ) ) {
          $next_article = @file_get_contents('https://mciggroup.com/json420news.php?post_id=' . $article->next_post, 0, $ctx);
          $next_article = json_decode( $next_article );
          if ( ! empty( $next_article ) && ! empty( $next_article->data ) ) {
            $article->next_post_slug = $this->slugify( $next_article->data[0]->title );
          }
        }
      }
    }
    return $article;
  }

  public function get_short_content( $content ) {
    $max_length = 500; // maximum number of characters to extra

    if ( strlen( $content ) <= $max_length) {
      return $content;
    }
    $trimmed_string = substr( $content, 0, $max_length );

    $trimmed_string = substr( $trimmed_string, 0, min( strlen( $trimmed_string ), strrpos( $trimmed_string, " " )));
    $trimmed_string = $trimmed_string . '...';
    return $trimmed_string;
  }

  public function getPost( $article ) {
    $pattern = '/^([ |\t]*?)([^ |\t].*?)([ |\t]*?)$/m';
    $article->content = preg_replace( $pattern, "<p class='investors-list__item-text'>$2</p>", $article->content );
    $date = new \DateTime( $article->date );
    $article->date = $date->format('F d');
    $article->date = new \stdClass();
    $article->date->year = $date->format('Y');
    $article->date->month = $date->format('M');
    $article->date->day = $date->format('d');
    $article->description = $this->get_short_content( strip_tags( $article->content ) );
    $article->slug = $this->slugify( $article->title );
    $article->url = "https://" . $_SERVER['SERVER_NAME'] . '/investors-article/' . $article->id . '/' . $article->slug . '/';
    $article->share = new \stdClass();
    $article->share->title = urlencode( $article->title );
    $article->share->description = urlencode( $article->description );
    $article->share->url = urlencode( $article->url );
  
    return $article;
  }

  public function generateShareLinks( $share ) {
    $links = array();
    $links['facebook'] = [
      'link'  => 'https://www.facebook.com/sharer.php?s=100&p[title]='. $share->title .'&u='. $share->url
              .'&t='. $share->title .'&p[summary]='. $share->description .'&p[url]='. $share->url,
      'class' => 'fb',
    ];
  
    $links['twitter'] = [
      'link'  => 'https://twitter.com/intent/tweet?url='. $share->url .'&text='. $share->description,
      'class' => 'tw',
    ];

    $links['email'] = [
      'link'  => "mailto:type%20email%20address%20here?subject=I%20wanted%20to%20share%20this%20post%20with%20you%20from%20Obitx%2Ecom&body=". urldecode( $share->title ) ."%20%20%3A%20%20". urldecode( $share->description ) ."%20%20%2D%20%20%28%20". $share->url ."%20%29",
      'class' => 'em',
    ];
  
    return $links;
  }

  public function slugify( $text ) {
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, '-');

		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
		  return 'n-a';
		}

		return $text;
	}
}
