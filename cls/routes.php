<?php
namespace Obitx;
/**
 * routes
 *
 */
class routes {

	private $dispatcher;
	private $uri;
	private $httpMethod;
	private $page;
	private $vars;

	public function __construct( $httpMethod, $uri ) {
		$this->httpMethod = $httpMethod;
		$this->uri = $uri;
		$this->get_dispatcher();
		$this->strip_query();
		$this->route_processing();
	}

	public function get_page() {
		return $this->page;
	}

	public function get_vars() {
		return $this->vars;
	}

	private function get_dispatcher() {
		$this->dispatcher = \FastRoute\simpleDispatcher(function(\FastRoute\RouteCollector $r) {
			$r->addRoute('GET', '/', 'main.php');
			$r->addRoute('GET', '/team[/]', 'team.php');
			$r->addRoute('GET', '/videos[/]', 'videos.php');
			$r->addRoute('GET', '/about[/]', 'about.php');
			$r->addRoute('GET', '/investors[/[{page}[/[{date}[/]]]]]', 'investors.php');
			$r->addRoute('GET', '/investors-article[/[{post}[/[{slug}[/]]]]]', 'investors-article.php');

			// old urls
			$r->addRoute('GET', '/index.html', 'main.php');
			$r->addRoute('GET', '/team.html', 'team.php');
			$r->addRoute('GET', '/videos.html', 'videos.php');
			$r->addRoute('GET', '/about.html', 'about.php');
			$r->addRoute('GET', '/investors.html?page={page}', 'investors.php');
			$r->addRoute('GET', '/investors-article.html?post={post}', 'investors-article.php');
			
			// $r->addRoute('POST', '/ajax[/[{action}[/]]]', 'ajax');
		});
	}

	private function strip_query() {
		// Strip query string (?foo=bar) and decode URI
		if ( false !== $pos = strpos( $this->uri, '?' ) ) {
			$uri = substr( $this->uri, 0, $pos );
		}

		$this->uri = rawurldecode( $this->uri );
	}

	private function route_processing() {
		$routeInfo = $this->dispatcher->dispatch( $this->httpMethod, $this->uri );
		switch ( $routeInfo[0] ) {
			case \FastRoute\Dispatcher::NOT_FOUND:
				$this->page = '404.php';
				$this->vars = [];
				break;
			case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
				$allowedMethods = $routeInfo[1];
				$this->page = '404.php';
				$this->vars = [];
				break;
			case \FastRoute\Dispatcher::FOUND:
				$this->page = $routeInfo[1];
				$this->vars = [];
				foreach ( $routeInfo[2] as $v_key => $var ) {
					$this->vars[ $v_key ] = str_replace( '/', '', $var );
				}
				break;
		}
	}

}
